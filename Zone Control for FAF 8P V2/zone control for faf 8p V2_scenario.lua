version = 4
ScenarioInfo = {
    name = 'Zone Control for FAF 8P V2',
    description = 'Zone Control for 8 Players. see faforever.com/forums/viewtopic.php?t=3972 Units spawn regularly in zones you control. Destroy the turret on a zone to claim the zone as your own. Kills earn you money for upgrades. 8P version by johnie102.',
    type = 'skirmish',
    starts = true,
    preview = '',
    size = {512, 512},
    map = '/maps/Zone Control for FAF 8P V2/Zone Control for FAF 8P V2.scmap',
    save = '/maps/Zone Control for FAF 8P V2/Zone Control for FAF 8P V2_save.lua',
    script = '/maps/Zone Control for FAF 8P V2/Zone Control for FAF 8P V2_script.lua',
    norushradius = 0.000000,
    norushoffsetX_ARMY_1 = 0.000000,
    norushoffsetY_ARMY_1 = 0.000000,
    norushoffsetX_ARMY_2 = 0.000000,
    norushoffsetY_ARMY_2 = 0.000000,
    norushoffsetX_ARMY_3 = 0.000000,
    norushoffsetY_ARMY_3 = 0.000000,
    norushoffsetX_ARMY_4 = 0.000000,
    norushoffsetY_ARMY_4 = 0.000000,
    norushoffsetX_ARMY_5 = 0.000000,
    norushoffsetY_ARMY_5 = 0.000000,
    norushoffsetX_ARMY_6 = 0.000000,
    norushoffsetY_ARMY_6 = 0.000000,
    norushoffsetX_ARMY_7 = 0.000000,
    norushoffsetY_ARMY_7 = 0.000000,
    norushoffsetX_ARMY_8 = 0.000000,
    norushoffsetY_ARMY_8 = 0.000000,
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'ARMY_1','ARMY_2','ARMY_3','ARMY_4','ARMY_5','ARMY_6','ARMY_7','ARMY_8',} },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_9' ),
            },
        },
    }}
