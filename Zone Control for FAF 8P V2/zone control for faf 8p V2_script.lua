--##****************************************************
--##**	ZONE CONTROL FOR FORGED ALLIANCE    		  **
--##**			        BY JOHNIE102                  **
--##**			       V 2.0                          **
--##****************************************************

-- MAP BY ANGRY ZEALOT AND JOHNIE102
-- EDITS BY KASPER
-- FURTHER EDITED BY JOHNIE102 (MADE 8 PLAYER VERSION FROM 4 PLAYER VERSION)

--scenario utilities
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local Utilities = import('/lua/utilities.lua')

--interface utilities
-- local UIUtil = import('/lua/ui/uiutil.lua')

--TRIGGER TABLES
local bunkerDeath = {}
local unitDeath = {}
	

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~GAME VARIABLES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


local executing = false
local mainBeatTime = 1
local spawnBeatTime = 5
local zones = {}
local xzonesize = 46.5
local yzonesize = 46.5
local xmapsize = ScenarioInfo.size[1]
local ymapsize = ScenarioInfo.size[2]
local zonexoffset = 1
local zoneyoffset = 2
local x = 0
local y = 0
local imax = math.floor(xmapsize / xzonesize)
local jmax = math.floor(ymapsize / yzonesize)
local mainBunkerType = 'ueb2301'
local players = {}
local defeatcount = 0
local gameEnd = false
local beat = 0


--PLAYERS, CONTAINS USEFUL INFO FOR THE SCRIPT FOR EACH PLAYER
defaultPlayersDATA = {
	kills = 0, 
	bunkerType = 'ueb2301',
	zonecontrol = 1, 
	attack = 0, 
	defence = 0, 
	kamikazesBought = 0,
	upgrades = 1, 
	level = 0,
	money = 0,
	infoUnit = false,
	attackUnit = false,
	defenceUnit = false,
	upgrader = false,
	kamikazeShop = false,
	nuke = false,
	expnuke = false,
	defeated = false,
}

function newplayersDATA(t)
	for key, val in pairs(defaultPlayersDATA) do
		if not t[key] then
			t[key] = val
		end
	end
	return t
end

local playersDATA = {
	['player1'] = newplayersDATA{name='player1', startLocation={5,2}, baseLocation={6,1}, baseType=1, upgradepos = {23,23}},
	['player2'] = newplayersDATA{name='player2', startLocation={3,4}, baseLocation={4,3}, baseType=2, upgradepos = {23,23}},
	['player3'] = newplayersDATA{name='player3', startLocation={2,7}, baseLocation={1,6}, baseType=1, upgradepos = {23,23}},
	['player4'] = newplayersDATA{name='player4', startLocation={4,9}, baseLocation={3,8}, baseType=2, upgradepos = {23,23}},
	['player5'] = newplayersDATA{name='player5', startLocation={7,10},baseLocation={6,11},baseType=1, upgradepos = {23,23}},
	['player6'] = newplayersDATA{name='player6', startLocation={9,8}, baseLocation={8,9}, baseType=2, upgradepos = {23,23}},
	['player7'] = newplayersDATA{name='player7', startLocation={10,5},baseLocation={11,6},baseType=1, upgradepos = {23,23}},
	['player8'] = newplayersDATA{name='player8', startLocation={8,3}, baseLocation={9,4}, baseType=2, upgradepos = {23,23}},
	['neutral'] = newplayersDATA{name='neutral'},
}

local baseTemplates = {
	{info = {23,9}, attack = {9,23}, defence = {35,23}, kamikazeShop = {23,37}, nukeLauncher = {35,37}, expLauncher = {9,37}, intel = {35,9}},
	{info = {9,9}, attack = {37,9}, defence = {9,37}, kamikazeShop = {37,37}, nukeLauncher = {23,37}, expLauncher = {23,9}, intel = {37,23}},
}

local baseLocations = {}
for _,t in pairs(playersDATA) do
	if t.name != "neutral" then
		table.insert(baseLocations, t.baseLocation)
	end
end

local armyTable = {	['ARMY_1'] = 'player1',['ARMY_2'] = 'player2',['ARMY_3'] = 'player3',['ARMY_4'] = 'player4',
					['ARMY_5'] = 'player5',['ARMY_6'] = 'player6',['ARMY_7'] = 'player7',['ARMY_8'] = 'player8',['ARMY_9'] = 'neutral',
}

local upgradeList = {
	['LEVEL 1'] = {level = 1, kills = 50, upgrade = 2, description = 'LEVEL 1: TANK 400 HP'},   
	['LEVEL 2'] = {level = 2, kills = 150, upgrade = 3, description = 'LEVEL 2: PILLAR 600 HP'},
	['LEVEL 3'] = {level = 3, kills = 400, upgrade = 6, description = 'LEVEL 3: ASSAULT BOT 1400 HP'},
	['LEVEL 4'] = {level = 4, kills = 800, upgrade = 7, description = ''},
	['LEVEL 5'] = {level = 5, kills = 1000, upgrade = 8, description = ''},
	['LEVEL 6'] = {level = 6, kills = 1200, upgrade = 9, description = ''},
	['LEVEL 7'] = {level = 7, kills = 1400, upgrade = 10, description = ''},
	['LEVEL 8'] = {level = 8, kills = 1600, upgrade = 10, description = ''},
	['LEVEL 9'] = {level = 9, kills = 1800, upgrade = 10, description = ''},
}
local upgradeListInsanity = {
	['LEVEL 1'] = {level = 1, kills = 1, upgrade = 2, description = 'LEVEL 1: TANK 400 HP'},   
	['LEVEL 2'] = {level = 2, kills = 2, upgrade = 3, description = 'LEVEL 2: PILLAR 600 HP'},
	['LEVEL 3'] = {level = 3, kills = 3, upgrade = 6, description = 'LEVEL 3: ASSAULT BOT 1400 HP'},
	['LEVEL 4'] = {level = 4, kills = 20, upgrade = 7, description = ''},
	['LEVEL 5'] = {level = 5, kills = 40, upgrade = 8, description = ''},
	['LEVEL 6'] = {level = 6, kills = 60, upgrade = 9, description = ''},
	['LEVEL 7'] = {level = 7, kills = 100, upgrade = 10, description = ''},
	['LEVEL 8'] = {level = 8, kills = 150, upgrade = 10, description = ''},
	['LEVEL 9'] = {level = 9, kills = 200, upgrade = 10, description = ''},
}

local unitUpgradeList = {
	['LEVEL 0'] = 'UEL0106',--level 0: mech marine
	['LEVEL 1'] = 'UEL0201',--level 1: striker
	['LEVEL 2'] = 'UEL0202',--level 2: pillar
	['LEVEL 3'] = 'UEL0303',--level 3: assault bot
	['LEVEL 4'] = 'UEL0303',--level 4: assault bot
	['LEVEL 5'] = 'UEL0303',--level 5: assault bot
	['LEVEL 6'] = 'UEL0303',--level 5: assault bot
	['LEVEL 7'] = 'UEL0303',--level 5: assault bot
	['LEVEL 8'] = 'UEL0303',--level 5: assault bot
	['LEVEL 9'] = 'UEL0303',--level 5: assault bot
}

local heroList = {
	[4] = {unit='ual0401', health=99999, name='Galactic Colossus'},
	[5] = {unit='url0402', health=250000, name='Lord of Monkey Lords'},
	[6] = {unit='uel0401', health=100000, name='Fatman'},
	[7] = {unit='ueb2305', health=1, name='Nuke Launcher'},
	[8] = {unit='xsb2401', health=1, name='Bringer of Death'},
	[9] = {unit='', health=1, name='More Ammo for the Bringer of Death'},
}


--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~UTILITY FUNCTIONS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--given a player returns an army
toArmy = function(player)
	local army = false
	for i,v in armyTable do
		if v == player.name then
			army = i
		end
	end
	return army
end


--given a player returns a proper username
username = function(player)
	return GetArmyBrain(toArmy(player)).Nickname
end

--given a player returns the number of kills
armyKills = function(player)
	local kills = 0
	if player.name != 'neutral' then 
		local army = toArmy(player)
		kills = GetArmyBrain(army):GetArmyStat('Enemies_Killed',0.0).Value 
	end
	return kills	
end

--print a text scn way :p
--first arg is the text to print
--second arg is what player it concerns
--third arg is if it has to be printed to that player only or to all players
scnPrint = function(text,player,playeronly)

	local army = toArmy(player)
	local colorIndex = ScenarioInfo.ArmySetup[army].PlayerColor
	local color = import('/lua/GameColors.lua').GameColors.ArmyColors[colorIndex]

	-- WARN(army)
	-- WARN(GetFocusArmy())
	-- WARN(color)
	-- WARN(text)
	
	if not playeronly then
		playerspecific = false
	else
		playerspecific = playeronly
	end
	
	
	if playerspecific == true then
		if army == indexToArmy(GetFocusArmy()) then
			PrintText(text,20,color,spawnBeatTime,'center')
		end
	else
		PrintText(text,20,color,spawnBeatTime,'center') 
	end
end

--given a unit returns the army
scnArmy = function(unit)
	local armyIndex = unit:GetArmy()
	return indexToArmy(armyIndex)
end

--given an army index returns an army
--more efficient but does not work yet
indexToArmy = function(armyIndex)
	local army = ListArmies()[armyIndex]
	return army
end

--get the number of players
playerCount = function()
	local i = 0
	for index, player in players do
		i = i + 1
	end
	return i
end


--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~MAIN SCRIPT FUNCTIONS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--populating
function OnPopulate()
	InitializeArmies()
    ScenarioFramework.SetPlayableArea('Playfield', false)
	import('/lua/SimUtils.lua').GiveUnitsToPlayer = function() end
end


--start of map script
function OnStart(self)
	-- force victory condition to sandbox
	ScenarioInfo.Options.Victory = 'sandbox'

	-- set score calculation to show total kills
	for _, army in ListArmies() do
		local brain = GetArmyBrain(army)
		brain.YourArmy = armyTable[army]
		brain.CalculateScore = function(thisBrain)
			return thisBrain.ZCInfo[thisBrain.YourArmy].kills
		end
	end
	
	if ScenarioInfo.Options.UnitCap == nil then
		ScenarioInfo.Options.UnitCap = '1000'
	end
	if ScenarioInfo.Options.zone_control_turret == nil then
		ScenarioInfo.Options.zone_control_turret = 'mixed'
	end
	if ScenarioInfo.Options.zone_control_intel == nil then
		ScenarioInfo.Options.zone_control_intel = 'true'
	end
	if ScenarioInfo.Options.zone_control_dialog == nil then
		ScenarioInfo.Options.zone_control_dialog = 'true'
	end
	if ScenarioInfo.Options.zone_control_insanity == nil then
		ScenarioInfo.Options.zone_control_insanity = 'false'
	end
	
	if ScenarioInfo.Options.zone_control_insanity == 'true' then
		upgradeList = upgradeListInsanity
	end
	
	--initialise scenario
	ScenarioFramework.SetPlayableArea('Playfield', false)
	Utilities.UserConRequest("ui_ForceLifbarsOnEnemy")--show enemy life bars
	initialiseZones()
	
	--initialise player zone control
	for index, player in players do
		if player.startLocation != nil then
			changeOwner(zones[player.startLocation[1]][player.startLocation[2]],player.name)
		end
	end
	
	--spawn start bunkers
	spawnBunkers()
	
	--spawn start info units
	spawnUtilityUnits()
	
	--creates the dialog that shows the zones of each player
	createDialog()
	
	--start the different scenario threads
	startScenarioThreads()
	
end

--creates the different threads 
startScenarioThreads = function()
	--start main thread
	local mainThread = ForkThread(function(self)
		while gameEnd == false do
			--call main thread beat
			mainThreadBeat()
			--wait time between each beat
			WaitSeconds(mainBeatTime)
		end
		KillThread(self)
    end)
	
	--check for upgrades in a separate thread for better reaction time
	local spawnThread = ForkThread(function(self)
		while gameEnd == false do
			--call spawn thread beat
			spawnThreadBeat()
			--wait time between each beat
			WaitSeconds(spawnBeatTime)			
			--increment beat count
			beat = beat + 1
		end
		KillThread(self)
	end)
end

--MAIN SCENARIO THREAD
--do map script actions every TICK
--this thread does all the checking of upgrade requests, zone ownership change and victory conditions?
mainThreadBeat = function()
	checkUpgrades()			
	checkZones()
	checkVictoryConditions()
	copyPlayerInfo()
end

--this thread spawns units
spawnThreadBeat = function()
	spawnUnits()
end

--copy the map script variables into the aibrains so that the zone control ai can use them
copyPlayerInfo = function()
	for i, player in players do
		GetArmyBrain(toArmy(player)).ZCInfo = table.copy(players)
	end
end


createDialog = function()
	if not  (ScenarioInfo.Options.zone_control_dialog == 'true') then
		return
	end
	local dialogThread = ForkThread(function(self)
			local infoDialog = CreateDialogue("Blaat",{"toggle on/off"},'right')
			infoDialog.toggled = true
			infoDialog.OnButtonPressed = function(self, button)
				if self.toggled then self.toggled = false
				else self.toggled = true
				end
			end
			while not gameEnd do
				updateDialog(infoDialog)
				WaitSeconds(1)
			end
			KillThread(self)
		end)
end

dialogSort = function(p1,p2)
	return (p1.zonecontrol > p2.zonecontrol)
end


updateDialog = function(dialog)
	local text = "Zone Controls: \n"
	local ordered = {}
	for name,player in pairs(players) do
		if not(player.zonecontrol == 0) then
			table.insert(ordered, player)
		end
	end
	table.sort(ordered,dialogSort)
	for i, player in ipairs(ordered) do
		text = text .. username(player) .. ": " .. player.zonecontrol .. " zones\n"
	end
	if dialog.toggled then
		dialog:SetText(text)
	else
		dialog:SetText("Toggled Off")
	end
end

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~MAIN THREAD FUNCTIONS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--check for end game conditions
checkVictoryConditions = function()
	--check for defeat
	for index, player in players do
		--LOG(player.zonecontrol)
		if player.zonecontrol == 0 and player.defeated  == false and player.name != 'neutral' then
			if ScenarioInfo.Options.zone_control_intel == "true" then
				spawnIntelBuilding(player)
			end
			player.defeated = true
			GetArmyBrain(toArmy(player)):OnDefeat()
		end
	end
	--check for victory
	if gameEnd == false then
		local playersUndefeated = {}
			
		--create a table with all undefeated players
		for index, player in players do
			if player.defeated == false and player.name != 'neutral' then
				table.insert(playersUndefeated,player)
			end
		end

		--check the alliances of the undefeated players and the number of undefeated players to determine if they must be victorious or not
		local n = table.getn(playersUndefeated)
		local stillFighting = false
		for i,army in pairs(playersUndefeated) do
			if i != n then
				if not IsAlly(toArmy(army),toArmy(playersUndefeated[i+1])) then
					stillFighting = true
				end
			end
		end
		if not stillFighting then
			gameEnd = true
			for i,army in pairs(playersUndefeated) do
				GetArmyBrain(toArmy(army)):OnVictory()
			end
		end
	end
end


--CHECK IF UPGRADES ARE NECESSARY, APPLY THEM
checkUpgrades = function()

	--Check for kill triggered upgrades
	for levelindex,level in upgradeList do
		for index,player in players do
			if player.kills >= level.kills then
				upgrade(player,levelindex)
			end
		end
	end
	
	--Check for area triggered upgrades
	checkUpgradeRequests()
end

--SPAWN UNITS ON ALL ZONES DEPENDING ON OWNERSHIP
spawnUnits = function()
	local i = 1
	while i <= imax do
		local j = 1
		while j <= jmax do
			spawnUnit(zones[i][j])
			j = j + 1
		end
		i = i + 1
	end	
end

--SPAWN BUNKERS AT START
spawnBunkers = function()
	local i = 1
	while i <= imax do
		local j = 1
		while j <= jmax do
			local spawn = true
			for _,pos in pairs(baseLocations) do
				if pos[1] == i and pos[2] == j then spawn=false end
			end
			if spawn then spawnBunker(zones[i][j]) end
			j = j + 1
		end
		i = i + 1
	end	
end

--CHECK OWNERSHIP ON ALL ZONES
checkZones = function()
	local i = 1
	while i <= imax do
		local j = 1
		while j <= jmax do
			checkZone(zones[i][j])
			j = j + 1
		end
		i = i + 1
	end	
end

--make all zones
initialiseZones = function()
	local i = 1
	while i <= imax do		
		local j = 1
		zones[i] = {}
		bunkerDeath[i] = {}
		while j <= jmax do
				zones[i][j] = createZone(i,j)
			j = j + 1
		end
		i = i + 1

	end
end



--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ZONE MANAGING FUNCTIONS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--zone constructor
createZone = function(i,j)
	--create zone at (i, j) coordonates
	local zone =  {owner = 'neutral', x = i, y = j, Xsize = xzonesize, Ysize = yzonesize, hasBunker = false}
	
	--create zone rectangle
	zone.rectangle = {}
	zone.rectangle.x0 = (zone.x - 1) * zone.Xsize + zonexoffset
	zone.rectangle.y0 = (zone.y - 1) * zone.Ysize + zoneyoffset
	zone.rectangle.x1 = zone.rectangle.x0 + zone.Xsize
	zone.rectangle.y1 = zone.rectangle.y0 + zone.Ysize
	
	--generate ,bunker death trigger functions
	bunkerDeath[i][j] = function()		
		--acknowledge bunker destruction and refresh zone count
		zone.hasBunker = false
		removeZone(zone.owner)		
	end

	return zone
end

--given a zone, returns the position of its center
zoneCenter = function(zone)
	local center = {}
	center.x = (zone.rectangle.x0 + zone.rectangle.x1)/2
	center.y = (zone.rectangle.y0 + zone.rectangle.y1)/2
	return center
end

-- This function returns true if units can get to this zone position (so a bunker should be placed on it)
isPlayableZone = function(i,j)
	-- this next line works because the map is a 'circle'
	if math.abs(i-6) + math.abs(j-6) > 5 then return false end
	for _,pos in baseLocations do
		if i==pos[1] and j==pos[2] then return false end -- the current position is a base location and thus isn't accesable
	end
	return true
end

--change the owner of a zone
changeOwner = function(zone,newowner)
	zone.owner = newowner
end

--returns the player that owns the zone
owner = function(zone)
	return players[zone.owner]
end


--CHECK ZONE OWNERSHIP, CHANGE IT IF NECESSARY
checkZone = function(zone)
	--IF ZONE CONTAINS A BUNKER DO NOT CHECK: DONE
	--IF ZONE CONTAINS ONLY UNITS FROM 1 PLAYER AND PLAYER != OWNER THEN OWNER = PLAYER	
	--CREATE A BUNKER FOR THE NEW OWNER: DONE	
	
	--if zone does not have a bunker
	if zone.hasBunker == false and isPlayableZone(zone.x,zone.y) then --and zone.owner == 'neutral' then
		--find new owner
		local ownerfound = refreshOwner(zone)
		--spawn bunker on zone
		if zone.owner != 'neutral' and ownerfound == true then
			if spawnBunker(zone) then
				--add zone to owner
				addZone(zone.owner)
			end
		end

	end	

end

--FIND NEW OWNER FOR ZONE
refreshOwner = function(zone)
	local ownerfound = false
	
	if not isPlayableZone(zone.x,zone.y) then return false end
	
	local ally = toArmy(owner(zone))
	local count = { ['ARMY_1'] = 0, ['ARMY_2'] = 0, ['ARMY_3'] = 0, ['ARMY_4'] = 0, ['ARMY_5'] = 0, ['ARMY_6'] = 0, ['ARMY_7'] = 0, ['ARMY_8'] = 0, ['ARMY_9'] = 0}
	local enemyCount = 0
	local allyCount = 0
	
	local unitsInZone = GetUnitsInRect(zone.rectangle)
	
	--get army counts in zone
	if unitsInZone then
		for k,unit in unitsInZone do
			if not unit:IsDead()  then
				local army = ''
				if unit:GetArmy() == playerCount() then
					army = 'ARMY_9'
				else
					army = scnArmy(unit)
				end
				
				count[army] = count[army] + 1
				if army != ally and IsAlly(ally, army) == false then 
					enemyCount = enemyCount + 1
				else
					allyCount = allyCount + 1
				end
			end
		end
	end
	
	--determine new owner depending on army count
	--zone owner retakes ownership
	if allyCount >= 10 and enemyCount == 0 then
		ownerfound = true
	--attacker captures zones
	elseif allyCount <= 5 then
		local maxcountArmy = false
		local maxvalue = 0
		local draw = false
		
		--find army with most troops on zone
		for army,value in count do 
			if maxcountArmy == false then
				maxcountArmy = army
			end
			if value > maxvalue then
				maxvalue = value
				maxcountArmy = army
				draw = false
			elseif value == maxvalue and value != 0 then
				draw = true
			end
		end
		
		--if the army with more troops has more than 5 troops then capture the zone
		if count[maxcountArmy] >= 5 and draw == false then
			zone.owner = armyTable[maxcountArmy]
			ownerfound = true
		end
		
		
	end
	
	return ownerfound
end


--add zone to count of player
addZone = function(playername)
	players[playername].zonecontrol = players[playername].zonecontrol + 1
	--instigator:GetAIBrain():GiveResource('MASS', mamount) 
	--armyTable:GetAIBrain():GiveResource('ENERGY', eamount) 
	--GetArmyBrain(toArmy(players[playername])):GiveResource('MASS', 1) 
	--LOG("" .. players[player].name .."ZONE CONTROL: " .. players[player].zonecontrol)
end

--remove zone from count of player
removeZone = function(playername)
	players[playername].zonecontrol = players[playername].zonecontrol - 1
	--GetArmyBrain(toArmy(players[playername])):TakeResource('MASS', 1) 
	--self:GetAIBrain():TakeResource('Energy', self:GetBlueprint().Economy.StorageEnergy) 
	--LOG("" .. players[player].name .."ZONE CONTROL: " .. players[player].zonecontrol)
end

--returns a valid spot to spawn a unit for a player in case he doesnt own his start location anymore
findZone = function(player)
	local startzone = zones[player.startLocation[1]][player.startLocation[2]]
	local properzone = startzone
	
	--if the player does not own his start zone anymore
	if startzone.owner != player.name then
		local mindistance = VDist2(1,1,imax,jmax) --initialise the minimum distance with the maximum value
		
		for i,zoneline in zones do
			for j,zone in zoneline do --calculate the minimal distance and save the corresponding zone
				if zone.owner == player.name and isPlayableZone(i,j) then
					zonedistance = VDist2(startzone.x, startzone.y, i, j)--math.sqrt( (i-startzone.x)*(i-startzone.x) + (j - startzone.y)*(j - startzone.y))
					if  zonedistance < mindistance then
						properzone = zone
						mindistance = zonedistance
						--LOG("temp min distance: " .. mindistance .. " for zone(" .. zone.x .. ")(".. zone.y .. ")")
					end
				end
			end
		end
	end
	
	return properzone
end






--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~UNIT SPAWNING FUNCTIONS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


--spawns a bunker on a zone
spawnBunker = function(zone)
	--SPAWN UNIT ON ZONE DEPENDING ON PLAYER OWNERSHIP
	--SPAWN NO UNIT IF ZONE IS NEUTRAL
	local x = zoneCenter(zone).x
	local y = zoneCenter(zone).y
	local player = owner(zone)
	local army = toArmy(player)
	
	zone.bunker = false
	if not isPlayableZone(zone.x,zone.y) then return end
	
	--spawn bunker if not population limit
	if GetArmyUnitCostTotal(GetArmyBrain(army):GetArmyIndex()) < GetArmyUnitCap(GetArmyBrain(army):GetArmyIndex()) then		
		if zone.x == player.startLocation[1] and zone.y == player.startLocation[2] then
			zone.bunker = CreateUnitHPR( mainBunkerType, toArmy(player), x,y,y, 0,0,0) 
		else
			zone.bunker = CreateUnitHPR( player.bunkerType, toArmy(player), x,y,y, 0,0,0) 
		end
	end
	
	--if bunker exists apply balance changes and upgrades
	if zone.bunker != false then
		zone.hasBunker = true
		
		if zone.owner == 'neutral' then 
			zone.bunker:SetMaxHealth(600)
			zone.bunker:SetHealth(zone.bunker,600)
		else
			zone.bunker:SetMaxHealth(5000 + 1000*player.defence)
			zone.bunker:SetHealth(nil,5000 + 1000*player.defence)
			zone.bunker:SetRegenRate(2*player.defence*player.defence)
													
			for i = 1, zone.bunker:GetWeaponCount() do
				zone.bunker:GetWeapon(i):ChangeRateOfFire(1 + player.defence/4)
				zone.bunker:GetWeapon(i):AddDamageMod(10*player.defence + 5*player.defence*player.defence)
			end
			
			zone.bunker:SetCustomName(username(player) .. ', level ' .. player.defence)
			
			addBunkerKillCount(zone.bunker)
		end
		zone.bunker:GetWeapon(1):ChangeMaxRadius(28)
		--disable wreckage
		zone.bunker:GetBlueprint().Wreckage.WreckageLayers.Land=false
		--death trigger
		ScenarioFramework.CreateUnitDeathTrigger(bunkerDeath[zone.x][zone.y],zone.bunker)
		
		ForkThread(function() playBuildEffect(zone.bunker) end)
	end
	return zone.bunker -- is false if no bunker is spawned

end

--ADD A KILL COUNT TO A BUNKER
--defender gets more money than attacker, a kill made by a bunker gives 10 instead of 5 for a regular kill
addBunkerKillCount = function(unit)
	--kill count
	unit.OldOnKilledUnit = unit.OnKilledUnit
	unit.OnKilledUnit = function(self, unitKilled)
		local player = players[armyTable[scnArmy(self)]]
		local enemy = players[armyTable[scnArmy(unitKilled)]]
		--if the unit did not commit suicide		
		if self:GetArmy() != unitKilled:GetArmy() then 
			player.kills = player.kills + 1
			if enemy.level == 2 then
				player.money = player.money + 15
			elseif enemy.level >= 3 then
				player.money = player.money + 20
			else
				player.money = player.money + 10
			end
			--update display
			updateInfo(player)			
			--GetArmyBrain(toArmy(player)):GiveResource('ENERGY', 10000) 		
		end
		unit.OldOnKilledUnit(self, unitKilled)	
	end		
end


--SPAWN UNIT ON ZONE
spawnUnit = function(zone)
	--SPAWN UNIT ON ZONE DEPENDING ON PLAYER OWNERSHIP
	--SPAWN NO UNIT IF ZONE IS NEUTRAL
	local x = zoneCenter(zone).x
	local y = zoneCenter(zone).y
	local unit = false	
	local player = owner(zone)
	local army = toArmy(player)
	local unitType = unitUpgradeList['LEVEL ' .. player.level]
	
	local xside = math.random(0,1)
	local yside = math.random(0,1)
	local xoff = 0
	local yoff = 0
	
	if zone.hasBunker == false then
	elseif zone.owner != 'neutral' then
		--check unit cap to avoid bug		
		if GetArmyUnitCostTotal(GetArmyBrain(army):GetArmyIndex()) < GetArmyUnitCap(GetArmyBrain(army):GetArmyIndex()) then
			--adjust spawn rate depending on level
			if player.level <= 1 then --low level units spawn every tick		
					unit = CreateUnitHPR( unitType, army, x+(-1+xside*2),25,y+(-1+yside*2), 0,0,0)	
			elseif player.level == 2 then -- pillars spawn 1 time out of 2
				if math.mod(beat,2) == 0 then	
					unit = CreateUnitHPR( unitType, army, x+(-1+xside*2),25,y+(-1+yside*2), 0,0,0)
				end
			elseif player.level >=3 then -- siege bots spawn 1 time out of 3
				if math.mod(beat,3) == 0 then
					unit = CreateUnitHPR( unitType, army, x+(-1+xside*2),25,y+(-1+yside*2), 0,0,0)
				end
			end
		end
	end
	
	--if unit exists apply unit balance changes and upgrades
	if unit != false then
		addKillCount(unit)
		
		unit:DestroyShield()
		unit:SetMaxHealth(200*upgrades(zone)+50*player.attack)
		unit:SetHealth(nil,200*upgrades(zone)+50*player.attack)
		
		--thanks to UArchitect
		--disable wreckage as way too many of the stuff appears..decals are enough, (wouldnt be so bad if i could make them slowly sink into the ground after a while)
		unit:GetBlueprint().Wreckage.WreckageLayers.Land=false
		
        for i = 1, unit:GetWeaponCount() do
            unit:GetWeapon(i):AddDamageMod(5*player.attack)
        end
		
		unit:SetSpeedMult(1.9)
		unit:SetAccMult(3)
        unit:SetTurnMult(3)
		
		--move unit after being created
		--thanks to UArchitect
		--add randomness stuff to the move order
		xoff = -8 + xside*16
		yoff = -8 + yside*16
		xoff = xoff + -2 + math.random(0,1)*4
		yoff = yoff + -2 + math.random(0,1)*4
		IssueMove( {unit}, {x+xoff,25,y+yoff} )
		
	end
end

--ADD A KILL COUNT TO A UNIT
addKillCount = function(unit)
	--kill count
	unit.OldOnKilledUnit = unit.OnKilledUnit
	unit.OnKilledUnit = function(self, unitKilled)
		local player = players[armyTable[scnArmy(self)]]
		local enemy = players[armyTable[scnArmy(unitKilled)]]
		--if the unit did not commit suicide		
		if self:GetArmy() != unitKilled:GetArmy() then 
			player.kills = player.kills + 1
			if enemy.level == 2 then
				player.money = player.money + 10
			elseif enemy.level >= 3 then
				player.money = player.money + 15
			else
				player.money = player.money + 5
			end
			--update display
			updateInfo(player)
		else
			player.kills = player.kills + 1.0/8.0
		end
		unit.OldOnKilledUnit(self, unitKilled)	
	end		
end


--UNIT DEATH TRIGGERS
unitDeath['player1'] = function()
	--players['player1'].deaths = players['player1'].deaths + 1
	WARN('player1')
	WARN('index: ' .. GetArmyBrain(toArmy(players['player1'])):GetArmyIndex())
end

unitDeath['player2'] = function()
	--players['player2'].deaths = players['player2'].deaths + 1
	WARN('player2')
	WARN('index: ' .. GetArmyBrain(toArmy(players['player2'])):GetArmyIndex())
end

unitDeath['player3'] = function()
	--players['player3'].deaths = players['player3'].deaths + 1
	WARN('player3')
	WARN('index: ' .. GetArmyBrain(toArmy(players['player3'])):GetArmyIndex())
end

unitDeath['player4'] = function()
	--players['player4'].deaths = players['player4'].deaths + 1
	WARN('player4')
	WARN('index: ' .. GetArmyBrain(toArmy(players['player4'])):GetArmyIndex())
end

unitDeath['player5'] = function()
	--players['player5'].deaths = players['player5'].deaths + 1
	WARN('player5')
	WARN('index: ' .. GetArmyBrain(toArmy(players['player5'])):GetArmyIndex())
end

unitDeath['player6'] = function()
	--players['player6'].deaths = players['player6'].deaths + 1
	WARN('player6')
	WARN('index: ' .. GetArmyBrain(toArmy(players['player6'])):GetArmyIndex())
end

unitDeath['player7'] = function()
	--players['player7'].deaths = players['player7'].deaths + 1
	WARN('player7')
	WARN('index: ' .. GetArmyBrain(toArmy(players['player7'])):GetArmyIndex())
end

unitDeath['player8'] = function()
	--players['player8'].deaths = players['player8'].deaths + 1
	WARN('player8')
	WARN('index: ' .. GetArmyBrain(toArmy(players['player8'])):GetArmyIndex())
end






--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~UPGRADE SYSTEM FUNCTIONS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

--for each player check each shop
checkUpgradeRequests = function()
	for index,player in players do
		local shops = {
			{unit = player.attackUnit, buy = function(player) upgradeAttack(player) end},
			{unit = player.defenceUnit, buy = function(player) upgradeDefence(player) end},
			{unit = player.kamikazeShop, buy = function(player) purchaseKamikaze(player) end},
		}
		for index2, shop in shops do
			checkShop(shop,player)
		end
	end
end

--check upgrade request on a shop
checkShop = function(shop,player)
	if shop.unit != false then	
		if player.upgrader then
			--get target data
			local v1 = player.upgrader:GetNavigator():GetGoalPos()
			local v2 = shop.unit:GetPosition()
			local check = {false,false,false}
			local tolerance = 10
			
			--verify if target is inside range
			for i = 1,3 do
				if math.abs(v1[i] - v2[i]) <= tolerance then
					check[i] = true
				end
			end

			--if check is ok do shop action
			if check[1] == true and check[2] == true and check[3] == true then			   
				ForkThread(function() playBuildEffect(shop.unit) end)					
				respawnUpgrader(player,player.upgradepos[1],player.upgradepos[2])
				shop.buy(player)
			end
		end
	end
end

--return the number of upgrades of the owner of a zone
upgrades = function(zone)
	return owner(zone).upgrades
end

--upgrades a player to a given level
upgrade = function(player,levelindex)
	if upgradeList[levelindex].level > player.level then
		player.level = upgradeList[levelindex].level
		player.upgrades = upgradeList[levelindex].upgrade
		
		--level specific actions
		spawnLevelHero(player,levelindex)
	end
end

disableVeterancy = function(unit)
	unit.CheckVeteranLevel = function(self) end
	unit.onKilledUnit = function(self,unitKilled) end
	unit.SetVeteranLevel = function(self, level) end
	unit.CreateVeterancyBuff = function(self, levelName, levelValue, buffType) end
end

--spawn hero unit if necessary given a player and a level
spawnLevelHero = function(player,levelindex)
	local spawnzone = findZone(player)
	local x = zoneCenter(spawnzone).x 
	local y = zoneCenter(spawnzone).y
	local l = upgradeList[levelindex].level
	local hero = nil
	if heroList[l] then
		if l < 7 then
			local xside = math.random(0,1)
			local yside = math.random(0,1)
			hero = CreateUnitHPR(heroList[l].unit, toArmy(player),x+(-1+xside*2),25,y+(-1+yside*2), 0,0,0)
			local xoff = -4 + xside*8
			local yoff = -4 + yside*8
			xoff = xoff + -1 + math.random(0,1)*2
			yoff = yoff + -1 + math.random(0,1)*2
			IssueMove({hero}, {x+xoff,25,y+yoff})
		elseif l < 9 then -- nuke launchers
			local pos = nil
			if l == 7 then 
				pos = baseTemplates[player.baseType].nukeLauncher
			else 
				pos = baseTemplates[player.baseType].expLauncher 
			end
			x = pos[1] + (player.baseLocation[1]-1)*xzonesize+zonexoffset
			y = pos[2] + (player.baseLocation[2]-1)*yzonesize+zoneyoffset
			hero = CreateUnitHPR(heroList[l].unit, toArmy(player), x,25,y, 0,0,0)
			hero:SetCanBeKilled(false)
			hero:SetDoNotTarget(true);
			hero:SetRegenRate(1)
			if l == 7 then 
				hero:GiveNukeSiloAmmo(2)
				player.nuke = hero
			else
				player.nuke:GiveNukeSiloAmmo(2)
				hero:GiveNukeSiloAmmo(1)
				player.expnuke = hero
			end
		elseif l == 9 then -- ammo for nuke launchers
			player.nuke:GiveNukeSiloAmmo(5)
			player.expnuke:GiveNukeSiloAmmo(1)
		end
		if l != 9 then
			hero:DestroyShield()
			hero:SetMaxHealth(heroList[l].health)
			hero:SetHealth(hero, heroList[l].health)
			hero:SetCustomName(heroList[l].name .. ": " .. username(player))
			disableVeterancy(hero)
		end
		scnPrint(username(player) .. ": " .. heroList[l].name .. " Has Arrived", player)
	else		
		scnPrint(username(player) .. " has reached " .. upgradeList[levelindex].description,player)
	end
end
		
		
--upgrade attack level trigger
--attack upgrade upgrades unit weapon damage
upgradeAttack = function(player)
	if player.money >= (player.attack + 1)*50 then
		player.money = player.money - (player.attack + 1)*50
		player.attack = player.attack + 1
		updateInfo(player)
		scnPrint(username(player) .. ': upgrading attack to level '..player.attack,player)
	else
		scnPrint(username(player) .. ': not enough money for attack upgrade',player,true)
	end
end

--upgrade defence level trigger
--defence upgrade upgrades unit health as well as tower regen rate
upgradeDefence = function(player)
	if player.money >= (player.defence + 1)*50 then
		player.money = player.money - (player.defence + 1)*50
		player.defence = player.defence + 1
		
		-- if player.defence == 8 then
			-- player.bunkerType = 'ueb2301'
			-- replaceBunkers(player)
		--elseif player.defence == 2 then
			--player.bunkerType = 'urb2301'
			--replaceBunkers(player)
		-- end
		
		updateInfo(player)
		scnPrint(username(player) .. ': upgrading defence to level '..player.defence,player)
		upgradeBunkers(player)
	else		
		scnPrint(username(player) .. ': not enough money for defence upgrade',player,true)
	end
end
	
--replace all bunkers to refresh bunker type
replaceBunkers = function(player)
	local i = 1
	while i <= imax do
		local j = 1
		while j <= jmax do
			local zone = zones[i][j]
			if zone.owner == player.name and zone.bunker != false then
				if not zone.bunker:IsDead() then
					zone.bunker:Kill()	
					if spawnBunker(zone) then
						addZone(zone.owner)	
					end
				end
			end
			j = j + 1
		end
		i = i + 1
	end	
end


--upgrades all alive bunkers for a player
upgradeBunkers = function(player)
	local i = 1
	while i <= imax do
		local j = 1
		while j <= jmax do
			local zone = zones[i][j]
			if zone.owner == player.name and zone.bunker != false then
				if not zone.bunker:IsDead() then
					ForkThread(function() playBuildEffect(zone.bunker) end)
					zone.bunker:SetRegenRate(12*(player.defence+1))
					zone.bunker:SetMaxHealth(5000 + 1000*player.defence + 70*player.defence*player.defence)
					zone.bunker:SetHealth(nil,5000 + 1000*player.defence)
					
					for i = 1, zone.bunker:GetWeaponCount() do
						zone.bunker:GetWeapon(i):ChangeRateOfFire(1 + player.defence/3.0)
						zone.bunker:GetWeapon(i):AddDamageMod(10*player.defence + 5*player.defence*player.defence)
					end
					
					zone.bunker:SetCustomName(username(player) .. ', level ' .. player.defence)
				end
			end
			j = j + 1
		end
		i = i + 1
	end	
end

--spawns a kamikaze unit if you have enough money
purchaseKamikaze = function(player)
	local unitcost = 100 + player.kamikazesBought*50
	if player.money >= unitcost then
		player.money = player.money - unitcost
		player.kamikazesBought = player.kamikazesBought + 1
		local spawnzone = findZone(player)
		local x = zoneCenter(spawnzone).x 
		local y = zoneCenter(spawnzone).y
		local kamikaze = CreateUnitHPR( 'URL0303',  toArmy(player), x,25,y + 5, 0,0,0) 
		local health = 2000 + 1000*player.attack
		kamikaze:SetMaxHealth(health)
		kamikaze:SetHealth(nil,health)
		kamikaze:SetAllWeaponsEnabled(false)
		kamikaze:SetSpeedMult(2)
		kamikaze:SetAccMult(3)       
        kamikaze:SetTurnMult(3)
		--addCzarKamikazeAbility(kamikaze)
		addKamikazeAbility(kamikaze)
		kamikaze:SetCustomName('kamikaze: ' .. username(player))
		updateInfo(player)
		scnPrint(username(player) .. ': bought a kamikaze',player)
	else
		scnPrint(username(player) .. ': not enough money to purchase a kamikaze',player,true)
	end
end

--Special ability kamikaze
addKamikazeAbility = function(unit)
	unit.OldOnKilled = unit.OnKilled
	unit.OnKilled = function(self, instigator, type, overkillRatio)
		local player = players[armyTable[scnArmy(self)]]
		local position = self:GetPosition()		
		local damage = 5000 + 1000*player.attack
		CreateLightParticle( self, -1, -1, 70, 62, 'flare_lens_add_02', 'ramp_red_10' )
		local deathThread = ForkThread(function()
			WaitSeconds(3)
			DamageArea(nil, position, 12, damage, 'Normal', true, true)		

		end)		
		unit.OldOnKilled(self, instigator, type, overkillRatio)	
	end	
end

--SPECIAL ABILITY: DEATH CZAR ATTACK uaa0310
addCzarKamikazeAbility = function(unit)
	unit.OldOnKilled = unit.OnKilled
	unit.OnKilled = function(self, instigator, type, overkillRatio)
		local player = players[armyTable[scnArmy(self)]]
		czar = CreateUnitHPR( 'uaa0310', scnArmy(self), self:GetPosition()[1],300,self:GetPosition()[3], 0,0,0)
		czar:GetBlueprint().Wreckage.WreckageLayers.Land=false
		local czardeathThread = ForkThread(function()
			WaitSeconds(5)
			czar:Kill()		
		end)
		unit.OldOnKilled(self, instigator, type, overkillRatio)
	end	
end

--spawns a kamikaze unit if you have enough money
purchaseArtillery = function(player)
	local unitcost = 200
	if player.money >= unitcost then
		player.money = player.money - unitcost
		local startzone = zones[player.startLocation[1]][player.startLocation[2]]
		local x = zoneCenter(startzone).x 
		local y = zoneCenter(startzone).y
		local artillery = CreateUnitHPR( 'UEL0304',  toArmy(player), x,25,y + 5, 0,0,0) 
		artillery:SetMaxHealth(3000)
		artillery:SetHealth(nil,3000)
		artillery:GetWeapon(1):ChangeMaxRadius(28)
		artillery:SetCustomName('artillery: ' .. username(player))
		updateInfo(player)
		scnPrint(username(player) .. ': bought an artillery',player)
	else
		scnPrint(username(player) .. ': not enough money to purchase an artillery',player)
	end
end


--kill and respawn player upgrader  unit
respawnUpgrader = function(player,x,y)
	
	local positionX = xzonesize*(player.baseLocation[1]-1)
	local positionY = yzonesize*(player.baseLocation[2]-1)

	--destroy upgrader unit if necessary
	if player.upgrader != false then
		player.upgrader:SetCanBeKilled(true)
		player.upgrader:Kill()
		player.upgrader = false
	end	
	--spawn upgrader unit
	if player.upgrader == false then 
		player.upgrader = CreateUnitHPR( 'URL0107', toArmy(player), positionX+x,25,positionY+y, 0,0,0) 
		player.upgrader:SetAllWeaponsEnabled(false)	
		player.upgrader:SetCanBeKilled(false)
		player.upgrader:SetDoNotTarget(true)
		player.upgrader:SetMaxHealth(1)
		player.upgrader:SetHealth(nil,1)
		player.upgrader:SetRegenRate(1)
		player.upgrader:SetSpeedMult(2)
		player.upgrader:SetAccMult(10)       
        --player.upgrader:SetTurnMult(10)
		player.upgrader:SetCustomName(username(player) .. ': Upgrader')
	end
end

--spawn invincible units used to show stats, make upgrades etc
spawnUtilityUnits = function()
	if players['player1'] then
		spawnPlayerUtilityUnits(players['player1'])
	end
	if players['player2'] then
		spawnPlayerUtilityUnits(players['player2'])
	end
	if players['player3'] then
		spawnPlayerUtilityUnits(players['player3'])
	end
	if players['player4'] then
		spawnPlayerUtilityUnits(players['player4'])
	end
	if players['player5'] then
		spawnPlayerUtilityUnits(players['player5'])
	end
	if players['player6'] then
		spawnPlayerUtilityUnits(players['player6'])
	end
	if players['player7'] then
		spawnPlayerUtilityUnits(players['player7'])
	end
	if players['player8'] then
		spawnPlayerUtilityUnits(players['player8'])
	end
end

--spawn invincible units used to show stats, make upgrades etc for a player at a given zone position
spawnPlayerUtilityUnits = function(player)
	local positionX = xzonesize*(player.baseLocation[1]-1) + zonexoffset
	local positionY = yzonesize*(player.baseLocation[2]-1) + zoneyoffset
	local bt = baseTemplates[player.baseType]
	
	--info unit
	player.infoUnit = CreateUnitHPR( 'uab5202', toArmy(player), positionX+bt.info[1],25,positionY+bt.info[2], 0,0,0) 
	player.infoUnit:SetAllWeaponsEnabled(false)		
	player.infoUnit:SetUnSelectable(true)
	player.infoUnit:SetCanBeKilled(false)
	player.infoUnit:SetDoNotTarget(true)
	player.infoUnit:SetMaxHealth(1)
	player.infoUnit:SetHealth(nil,1)
	player.infoUnit:SetRegenRate(1)
	
	--attack upgrade
	player.attackUnit = CreateUnitHPR( 'urb5202', toArmy(player), positionX+bt.attack[1],25,positionY+bt.attack[2], 0,0,0) 
	player.attackUnit:SetAllWeaponsEnabled(false)		
	player.attackUnit:SetCanBeKilled(false)
	player.attackUnit:SetDoNotTarget(true)
	player.attackUnit:SetMaxHealth(1)
	player.attackUnit:SetHealth(nil,1)
	player.attackUnit:SetRegenRate(1)

	--defence upgrade
	player.defenceUnit = CreateUnitHPR( 'ueb5202', toArmy(player), positionX+bt.defence[1],25,positionY+bt.defence[2], 0,0,0) 
	player.defenceUnit:SetAllWeaponsEnabled(false)		
	player.defenceUnit:SetCanBeKilled(false)
	player.defenceUnit:SetDoNotTarget(true)
	player.defenceUnit:SetMaxHealth(1)
	player.defenceUnit:SetHealth(nil,1)
	player.defenceUnit:SetRegenRate(1)
		
	--kamikaze shop
	player.kamikazeShop = CreateUnitHPR( 'URB4201', toArmy(player), positionX+bt.kamikazeShop[1],25,positionY+bt.kamikazeShop[2], 0,0,0) 
	player.kamikazeShop:SetAllWeaponsEnabled(false)		
	player.kamikazeShop:SetCanBeKilled(false)
	player.kamikazeShop:SetDoNotTarget(true)
	player.kamikazeShop:SetMaxHealth(1)
	player.kamikazeShop:SetHealth(nil,1)
	player.kamikazeShop:SetRegenRate(1)
	player.kamikazeShop:SetScale(5)
	
	--upgrade selection unit
	respawnUpgrader(player,player.upgradepos[1],player.upgradepos[2])
	
	--resource storage unit, needed to have resource storage for stats like money and zone control
	--spawn resource storage units here
	
	updateInfo(player)
end

spawnIntelBuilding = function(player)
	local positionX = xzonesize*(player.baseLocation[1]-1) + zonexoffset
	local positionY = yzonesize*(player.baseLocation[2]-1) + zoneyoffset
	local bt = baseTemplates[player.baseType]
	
	local building = CreateUnitHPR('XRB3301', toArmy(player), positionX+bt.intel[1],25,positionY+bt.intel[2], 0,0,0)
	building:SetCanBeKilled(false)
	building:SetDoNotTarget(true)
	building:SetMaxHealth(1)
	building:SetHealth(nil,1)
	building:SetRegenRate(1)
	building:SetConsumptionPerSecondEnergy(0)
	
	bp = building:GetBlueprint()
	bp.Intel.MaxVisionRadius = 500
	bp.Intel.MinVisionRadius = 500
	bp.Intel.VisionRadius = 500
	building:SetIntelRadius('Vision', 500)
end
	

--update player info
updateInfo = function(player)	
	if player.infoUnit != false then
		player.infoUnit:SetCustomName('Money ' .. username(player) .. ": " .. player.money)
	end
	if player.attackUnit != false then
		player.attackUnit:SetCustomName('Attack level: ' .. player.attack ..', upgrade for: ' .. (player.attack + 1)*50)
	end
	if player.defenceUnit != false then
		player.defenceUnit:SetCustomName('Defence level: ' .. player.defence ..', upgrade for: ' .. (player.defence + 1)*50)
	end	
	if player.kamikazeShop != false then
		player.kamikazeShop:SetCustomName('Shop, purchase kamikaze for: ' .. (100+player.kamikazesBought*50))
	end
end


--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~MISCALENIOUS FUNCTIONS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


--SPECIAL ARMY INITIALISATION: NO COMMANDER, NEUTRAL ALWAYS ENEMY
function InitializeArmies()
    local tblArmy = ListArmies()
	
	--multiplayer lobby game option: chose turret type
	local bunkerType = 'uab2301'
	if 	ScenarioInfo.Options.zone_control_turret == 'mixed' then
		mainBunkerType = 'ueb2301'
		bunkerType = 'uab2301'
	elseif ScenarioInfo.Options.zone_control_turret == 'triads' then
		mainBunkerType = 'ueb2301'
		bunkerType = 'ueb2301'
	elseif ScenarioInfo.Options.zone_control_turret == 'oblivions' then
		mainBunkerType = 'uab2301'
		bunkerType = 'uab2301'
	end

	
	--determine the players depending on which slots are occupied
	for index,army in tblArmy do
		players[armyTable[army]] = playersDATA[armyTable[army]]
		players[armyTable[army]].bunkerType = bunkerType
	end
	players['neutral'].bunkerType = 'urb2301'
	players['neutral'].zonecontrol = 53 - playerCount() + 1
	
	for i, Army in ListArmies() do
		if Army != "ARMY_9" then
			SetAlliance(Army, "ARMY_9", "Enemy")
			for j, Army2 in ListArmies() do
				if Army2 != "ARMY_9" and not IsAlly(Army, Army2) then
					SetAlliance(Army, Army2, "Enemy")
				end
			end
		end
	end
	
end

playBuildEffect = function(unit,wait)

	unit:PlayUnitSound('TeleportStart')
	unit:PlayUnitAmbientSound('TeleportLoop')
	WaitSeconds( 0.1 )
	unit:PlayTeleportInEffects()
	WaitSeconds( 0.1 ) 
    unit:StopUnitAmbientSound('TeleportLoop')
    unit:PlayUnitSound('TeleportEnd')

end
