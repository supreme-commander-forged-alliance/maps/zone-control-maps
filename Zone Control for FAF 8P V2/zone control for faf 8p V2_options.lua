options = {
    {
		default = 1,
        label = "Zone Control: Turret type",
        help = "Mixed means that the turret in the starting location is a Triad.",
        key = 'zone_control_turret',
        pref = 'zone_control_turret',
        values = {
            {
              text = "Mixed",
              help = "",
              key = 'mixed',
           },
            {
              text = "Triads",
              help = "",
              key = 'triads',
           },
            {
              text = "Oblivions",
              help = "",
              key = 'oblivions',
           },
       },
    },
	
	{
		default = "true",
		label = "Zone Control: Spectator mode",
		help = "Defeated players get to see everything",
		key = "zone_control_intel",
		pref = "zone_control_intel",
		values = {
			{text="true",help="",key = "true",},
			{text="false",help="",key = "false",},
		},
	},
	
	{
		default = "true",
		label = "Zone Control: Zone Dialog",
		help = "A dialog showing the amount of zones per player",
		key = "zone_control_dialog",
		pref = "zone_control_dialog",
		values = {
			{text="true",help="",key = "true",},
			{text="false",help="",key = "false",},
		},
	},
	
	{
		default = "false",
		label = "Zone Control: Insanity Mode",
		help = "Let's start a war! Let's start a nuclear war!",
		key = "zone_control_insanity",
		pref = "zone_control_insanity",
		values = {
			{text="false",help="",key = "false",},
			{text="true",help="",key = "true",},
		},
	},
   
};