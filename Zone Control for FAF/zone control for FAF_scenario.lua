version = 3
ScenarioInfo = {
  Configurations={
    standard={
      customprops={ ExtraArmies="ARMY_9" },
      teams={
        { armies={ "ARMY_1", "ARMY_2", "ARMY_3", "ARMY_4" }, name="FFA" }
      }
    }
  },
  description="Zone control for Supreme Commander brought to you by Saya, map by AngryZealot",
  map="/maps/zone control for FAF.v0001/zone control for FAF.scmap",
  map_version=1,
  name="Zone Control for FAF",
  norushradius=0,
  preview="",
  save="/maps/zone control for FAF.v0001/zone control for FAF_save.lua",
  script="/maps/zone control for FAF.v0001/zone control for FAF_script.lua",
  size={ 256, 256 },
  starts=true,
  type="skirmish"
}
