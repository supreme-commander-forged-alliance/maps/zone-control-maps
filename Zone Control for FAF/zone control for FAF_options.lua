options = {
    {
		default = 1,
        label = "Zone Control: Turret type",
        help = "",
        key = 'zone_control_turret',
        pref = 'zone_control_turret',
        values = {
            {
              text = "Mixed",
              help = "",
              key = 'mixed',
           },
            {
              text = "Triads",
              help = "",
              key = 'triads',
           },
            {
              text = "Oblivions",
              help = "",
              key = 'oblivions',
           },
       },
    },
   
} 